﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using UnityEngine;

public class SaveData {

    const string dirName = "/Files";
    static string directoryPath = string.Concat(Application.dataPath, dirName);

    const string fileName = "/data.xml";

    static string filepath = string.Concat(directoryPath, fileName);

    ///Delete a file
    public static void DeleteFile(string filepath)
    {
        File.Delete(filepath);
    }

    public static void Save(Data obj)
    {
        XmlSerializer serializer = new XmlSerializer(typeof(Data));

        Directory.CreateDirectory(directoryPath);

        using (FileStream file = File.Open(filepath, FileMode.OpenOrCreate))
        {
            serializer.Serialize(file, obj);
        }
        Debug.Log("saved file at " + filepath);
    }

    public static object Load()
    {
        Data data = new Data();
        XmlSerializer serializer = new XmlSerializer(typeof(Data));
        using (FileStream file = File.Open(filepath, FileMode.Open))
        {
            data = serializer.Deserialize(file) as Data;
        }
        return data;
    }
}

[System.Serializable]
public class Data
{
    public float duration;
    public float movespeed;
    
    public Data()
    {
        duration = 30;
        movespeed = 6;
    }
}