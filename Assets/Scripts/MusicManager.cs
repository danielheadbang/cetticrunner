﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour {

    bool muted = false;

    AudioSource audiosource;

    private void Awake()
    {
        audiosource = GetComponent<AudioSource>();
    }

    public void ToggleMute()
    {
        print("call");
        muted = !muted;
        audiosource.mute = muted;
    }
}
