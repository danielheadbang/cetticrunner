﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour {
	[Tooltip("How many points will be worth")]
	public float pointsWorth;
	[SerializeField, Tooltip("Should we disable this item after being interacted?")]
	bool disableOnInteraction = false;

    bool interacted = false;

    [SerializeField]
    float travelSpeed;

    [SerializeField]
    float destroyAfter;

    [SerializeField]
    AudioClip interactionSound;

    void Awake()
    {
        Destroy(gameObject, destroyAfter);
    }

	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.CompareTag(TagManager.Player)){
			if(!interacted)
            {
                GameManager.Instance.AddScore(pointsWorth);
                interacted = true;

                GameManager.Instance.PlaySound(interactionSound);
            }

			if(disableOnInteraction) gameObject.SetActive(false);
		}
	}

    void FixedUpdate()
    {
        transform.Translate(Vector2.left * travelSpeed);
    }
}
