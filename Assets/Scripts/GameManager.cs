﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {
	public static GameManager Instance;

    [Header("Core objects and values")]
	/// <summary>
    /// the game duration in seconds
    /// </summary>
	[Tooltip("The game duration in seconds")]
	public float gameDuration;
    public PlayerController controller; //We keep this stored here so we can update them easier on the editor

	bool isPaused;
    /// <summary>
    /// Is the game currently paused?
    /// </summary>
    public bool paused{
		get{
			return isPaused;
		}
	}
    
	[SerializeField, Tooltip("The text that will display the score")] 
	Text scoreText;
    /// <summary>
    /// Is the game currently paused?
    /// </summary>
    float currentScore = 0;
    /// <summary>
    /// The current score formated to string
    /// </summary>
    string scoreAsText
    {
		get{
			return currentScore.ToString();
		}
	}

    /// <summary>
    /// Is the game over?
    /// </summary>
	public bool gameOver = false;

    [Header("UI")]
    [SerializeField]
    GameObject gameoverPanel;
    [SerializeField]
    Text gameOverScoreText;
    [SerializeField]
    Text secondsPassedText;
    [SerializeField]
    GameObject pausePanel;

    AudioSource audioSource;

	void Awake()
	{
		Instance = this;
		scoreText.text = scoreAsText;
        audioSource = GetComponent<AudioSource>();
	}

    /// <summary>
    /// Specify the current pause state
    /// </summary>
    /// <param name="paused"></param>
    public void SetPause(bool paused){
		isPaused = paused;
        pausePanel.SetActive(paused);
        Time.timeScale = paused ? 0 : 1;
	}

    /// <summary>
    /// Toggle between paused and not paused
    /// </summary>
    public void TogglePause(){
		isPaused = !isPaused;
        pausePanel.SetActive(paused);
        Time.timeScale = paused ? 0 : 1;
    }

    /// <summary>
    /// Add score to our session
    /// </summary>
    /// <param name="score"></param>
    public void AddScore(float score){
		currentScore += score;
		scoreText.text = scoreAsText;
	}

	void Start()
	{
		StartCoroutine(WaitAndCountEachSecond());
	}

	IEnumerator WaitWholeDuration(){
		yield return new WaitForSeconds(gameDuration);
		gameOver = true;
	}

	IEnumerator WaitAndCountEachSecond(){
		float secondsPassed = 0;
        string timeLeftText = (gameDuration - secondsPassed).ToString();
        secondsPassedText.text = timeLeftText;
		while(secondsPassed<gameDuration){
            //print(num);
			yield return new WaitForSeconds(1f);
            while (isPaused) yield return null; //if we pause the game we wait until unpaused to resume countdown
            secondsPassed++;
            timeLeftText = (gameDuration - secondsPassed).ToString();
            secondsPassedText.text = timeLeftText;
		}
        Gameover();
	}

    /// <summary>
    /// Manage what happens when the game is over
    /// </summary>
    void Gameover()
    {
        gameOver = true;
        gameoverPanel.SetActive(true);
        Time.timeScale = 0f;
        gameOverScoreText.text = scoreAsText;
    }

    public void PlaySound(AudioClip clip)
    {
        audioSource.PlayOneShot(clip);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            TogglePause();
        }
    }
}
