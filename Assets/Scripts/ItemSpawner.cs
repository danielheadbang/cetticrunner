﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemSpawner : MonoBehaviour {

    [SerializeField]
    List<GameObject> items = new List<GameObject>();
    [SerializeField]
    float spawnEvery;
    [SerializeField]
    List<Transform> spawnPositions = new List<Transform>();

	// Use this for initialization
	void Start () {
        StartCoroutine(Spawn());
	}
	
    IEnumerator Spawn()
    {
        while (!GameManager.Instance.gameOver && !GameManager.Instance.paused)
        {
            yield return new WaitForSeconds(spawnEvery);
            int rand = Random.Range(0, spawnPositions.Count);
            int randomItem = Random.Range(0, items.Count);
            GameObject theItem = Instantiate(items[randomItem], spawnPositions[rand].position, Quaternion.identity);
            //GameObject itemObject = Instantiate(itemPrefab, spawnPositions[rand].position, Quaternion.identity);
            print("spawn");
        }
    }
}
