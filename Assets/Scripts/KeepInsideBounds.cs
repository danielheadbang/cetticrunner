﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeepInsideBounds : MonoBehaviour {
    [SerializeField]
    Transform bound1, bound2;
    Camera cam;

    [SerializeField]
    Vector2 bound1Pos, bound2Pos;

    // Use this for initialization
    void Start () {
        cam = GetComponent<Camera>();
        bound1.position = cam.ViewportToWorldPoint(bound1Pos);
        bound2.position = cam.ViewportToWorldPoint(bound2Pos);
    }
}
