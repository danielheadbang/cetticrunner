﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    public static PlayerController Instance;
    public float moveSpeed;
    public float jumpForce;

    bool canJump = false;

    [SerializeField]
    int jumps = 2;
    int currentJumps = 0;

    Rigidbody2D body;

    [SerializeField]
    ForceMode2D forceType = ForceMode2D.Impulse;

    float horizontal;

    Animator animator;

    SpriteRenderer spriteRenderer;

    private const string animatorRunning = "Running";
    private const string animatorJump = "Jump";

    void Awake()
    {
        Instance = this;
        body = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
    }

	void Update () {
        horizontal = Input.GetAxis(AxisManager.Horizontal);
#region Handling animations and sprite flipping
        if (horizontal != 0)
        {
            animator.SetBool(animatorRunning, true);
            if (horizontal > 0)
                spriteRenderer.flipX = false;
            else
                spriteRenderer.flipX = true;
        }
        else animator.SetBool(animatorRunning, false);
#endregion

        bool jumpAction = Input.GetButtonDown(AxisManager.Jump);
        if (jumpAction && canJump && currentJumps < jumps)
        {
            animator.SetTrigger(animatorJump);
            body.AddForce(Vector2.up * jumpForce, forceType);
            currentJumps++;
            if (currentJumps >= jumps) canJump = false;
        }
    }

    void FixedUpdate()
    {
        //body.velocity = new Vector2(moveSpeed, body.velocity.y);
        body.velocity = new Vector2(horizontal * moveSpeed, body.velocity.y);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.CompareTag(TagManager.Ground))
        {
            canJump = true;
            currentJumps = 0;
        }
    }
}
