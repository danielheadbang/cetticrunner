﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class UpdateMainValues : EditorWindow {

	[MenuItem("Tools/Update Game Manager")]
    public static void CallUpdate()
    {
        EditorGamanagerUpdater.Update();
    }

    [MenuItem("Tools/Create Default File")]
    public static void Create()
    {
        Data data = new Data();
        SaveData.Save(data);
    }
}

[CustomEditor(typeof(GameManager))]
public class EditorGamanagerUpdater : Editor
{
    static GameManager manager;

    public override void OnInspectorGUI()
    {
        manager = target as GameManager;
        base.OnInspectorGUI();
    }

    public static void Update()
    {
        if (manager == null)
        {
            Debug.LogError("Please select the game manager first");
            return;
        }
        //change the values we want from the game manager
        Data data = SaveData.Load() as Data;
        manager.gameDuration = data.duration;
        manager.controller.moveSpeed = data.movespeed;
        Debug.Log("File updated");
    }
}
